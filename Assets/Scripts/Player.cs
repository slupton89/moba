﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    [SerializeField]
    private float shootDistance;
    [SerializeField]
    private Transform bulletSpawnPoint;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private GameObject playerStar;
    [SerializeField]
    private TextMesh healthText;

    private Transform targetedEnemy;
    private bool enemyClicked;
    private bool isWalking;
    private Animator anim;
    private NavMeshAgent navAgent;
    private float nextFire;
    private float timeBetweenShots = 2;
    private bool isAttacking = false;
    [SyncVar(hook = "OnHealthChange")]private int health = 100;
    private int bulletDamage = 25;
    private Vector3 startingPos;

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();
        playerStar.SetActive(true);
        tag = "Player";
    }

    // Use this for initialization
    void Start () {
        Assert.IsNotNull(healthText);
        Assert.IsNotNull(bulletSpawnPoint);
        Assert.IsNotNull(bulletPrefab);
        Assert.IsNotNull(playerStar);

        anim = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        startingPos = transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer) {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Input.GetButtonDown("Fire2")) {
            if(Physics.Raycast(ray, out hit, 100)) {
                if(hit.collider.CompareTag("Enemy")) {
                    targetedEnemy = hit.transform;
                    enemyClicked = true;
                } else {
                    isAttacking = false;
                    isWalking = true;
                    enemyClicked = false;
                    navAgent.destination = hit.point;
                    navAgent.Resume();
                }
            }
        }
        if(enemyClicked) {
            MoveAndShoot();
        }

        if(navAgent.remainingDistance <= navAgent.stoppingDistance) {
            isWalking = false;
        } else {
            if(!isAttacking) {
                isWalking = true;
            }
        }

        //Debug.Log("WALKING: " + isWalking);
        anim.SetBool("IsWalking", isWalking);
	}

    void MoveAndShoot() {
        if(targetedEnemy == null) {
            return;
        }
        navAgent.destination = targetedEnemy.position;
        if(navAgent.remainingDistance >= shootDistance) {
            navAgent.Resume();
            isWalking = true;
        }

        if(navAgent.remainingDistance <= shootDistance) {
            transform.LookAt(targetedEnemy);
            if(Time.time > nextFire) {
                isAttacking = true;
                nextFire = Time.time + timeBetweenShots;
                CmdFire();
            }
            navAgent.Stop();
            isWalking = false;
        }
    }

    [Command]
    void CmdFire() {
        anim.SetTrigger("Attack");
        GameObject fireball = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation) as GameObject;
        fireball.GetComponent<Rigidbody>().velocity = fireball.transform.forward * 4;
        NetworkServer.Spawn(fireball);

        Destroy(fireball, 3.5f);
    }

    void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.CompareTag("Bullet")) {
            TakeDamage();
        }
    }

    void TakeDamage() {
        if (!isServer) {
            return;
        }
        health -= bulletDamage;

        if(health <= 0) {
            health = 100;
            RpcRespawn();
        }
    }

    void OnHealthChanged(int updatedHealth) {
        healthText.text = updatedHealth.ToString();
    }


    [ClientRpc]
    void RpcRespawn() {
        if(isLocalPlayer) {           
            transform.position = startingPos;
        }
    }
}
